from AthenaCommon.Logging import logging 
log = logging.getLogger("TrigInDetValidation")
log.info( "Starting postinclude: test_preinclude_NNclustering.py" ) 

# Here's the list of tools we have access to
print("ToolSvc:")
from AthenaCommon.AppMgr import ToolSvc
print(ToolSvc)

# This is what we use to access algorithms in the sequence
from AthenaConfiguration.ComponentAccumulator import CompFactory
print("CompFactory components")
print(dir(CompFactory))
myalg = CompFactory.Trk__TrkAmbiguitySolver("InDetTrigMTTrkAmbiguitySolver_bjet")
myalg2 = CompFactory.Trk__TrkAmbiguityScore("InDetTrigMTTrkAmbiguityScore_bjet")
myalg.unlock()
print("Inital ambiguity solver:")
print(myalg)
print("Inital score processor:")
print(myalg2)

# Make a new ambiguity processor
from InDetTrigRecExample.InDetTrigConfigRecLoadTools import InDetTrigTrackFitter
from TrkAmbiguityProcessor.TrkAmbiguityProcessorConf import Trk__DenseEnvironmentsAmbiguityProcessorTool as ProcessorTool
from InDetTrigRecExample.InDetTrigFlags import InDetTrigFlags
# Get everything needed for it
# Zhen: please check that these are the same tool names as in your own configuration!
# Hard to check without running - e.g. there are several scoring tools, is this the right one?
# Should the track summary tool be InDetTrigTrackSummaryTool or InDetTrigTrackSummaryToolSharedHits?
# And so forth....

# This seems a bit dumb but is the best I can do
from TrigInDetConfig.ConfigSettings import getInDetTrigConfig
IDTrigConfig = getInDetTrigConfig( 'bjet' )

#from TrigInDetConfig.InDetTrigCommon import ambiguityScoringTool_builder
from TrigInDetConfig.InDetPT import scoringTool_builder
#ambiguityScoringTool = ambiguityScoringTool_builder('InDetTrigMT_AmbiguityScoringTool_bjet', IDTrigConfig, trackSummaryTool=ToolSvc.InDetTrigTrackSummaryTool )
ambiguityScoringTool = scoringTool_builder('bjet', IDTrigConfig, summaryTool=ToolSvc.InDetTrigTrackSummaryTool ,prefix="InDetTrigMT_Ambiguity")
print("gogogo")
print(ambiguityScoringTool)
ToolSvc += ambiguityScoringTool
ToolSvc.InDetTrigAmbiTrackSelectionTool.doPixelSplitting=True
ambiguityProcessor = ProcessorTool(  name = "InDetTrigMT_DenseEnvironmentsAmbiguityProcessor_bjet",
                                            Fitter           = [ToolSvc.InDetTrigTrackFitter],
                                            AssociationTool  = ToolSvc.InDetTrigPRDtoTrackMapToolGangedPixels,
                                            ScoringTool      = ambiguityScoringTool,
                                            TrackSummaryTool = ToolSvc.InDetTrigTrackSummaryTool,
                                            SelectionTool    = ToolSvc.InDetTrigAmbiTrackSelectionTool,#InDetTrigAmbiTrackSelectionTool,
                                            SuppressHoleSearch = False,
                                            tryBremFit         = InDetTrigFlags.doBremRecovery(),
                                            caloSeededBrem     = False,
                                            
                                            RefitPrds          = True,
                                            pTminBrem          = 1000.0
                                        )

# Pop it into ToolSvc so we can access it later
ToolSvc += ambiguityProcessor
# --- new NN prob tool
MultiplicityContent = [1 , 1 , 1]
import InDetRecExample.TrackingCommon as TrackingCommon
from AthenaCommon.CfgGetter import getPublicTool
TrigPixelLorentzAngleTool = getPublicTool("PixelLorentzAngleTool")
#from InDetTrigRecExample.InDetTrigConfigRecLoadTools import TrigNnClusterizationFactory
TrigNnClusterizationFactory = TrackingCommon.getNnClusterizationFactory( name                         = "TrigNnClusterizationFactory",
                                                                      PixelLorentzAngleTool        = TrigPixelLorentzAngleTool,
                                                                      useToT                       = InDetTrigFlags.doNNToTCalibration(),
                                                                      NnCollectionReadKey          = 'PixelClusterNN',
                                                                      NnCollectionWithTrackReadKey = 'PixelClusterNNWithTrack',
                                                                      useTTrainedNetworks          = True)
ToolSvc += TrigNnClusterizationFactory
from SiClusterizationTool.SiClusterizationToolConf import InDet__NnPixelClusterSplitProbTool as PixelClusterSplitProbTool
TrigNnPixelClusterSplitProbTool=PixelClusterSplitProbTool(name       = "TrigNnPixelClusterSplitProbTool",
                                                                PriorMultiplicityContent = MultiplicityContent,
                                                                NnClusterizationFactory  = TrigNnClusterizationFactory,
                                                                useBeamSpotInfo          = True)
ToolSvc += TrigNnPixelClusterSplitProbTool

#TODO need to create the ambiguityScoreProcessorTool and add TrigNnPixelClusterSplitProbTool to AmbiguityScoreProcessorTool_bjet 

#DenseEnvironmentsAmbiguityScoreProcessorTool
from TrkAmbiguityProcessor.TrkAmbiguityProcessorConf import Trk__DenseEnvironmentsAmbiguityScoreProcessorTool
from InDetRecExample import TrackingCommon as TrackingCommon
trackMapTool = TrackingCommon.getInDetTrigPRDtoTrackMapToolGangedPixels()

ambiguityScoreProcessor = Trk__DenseEnvironmentsAmbiguityScoreProcessorTool( name               = "InDetTrigMT_AmbiguityScoreProcessorTool_bjet",
                                                                 ScoringTool        = ambiguityScoringTool,
                                                                 AssociationTool    = trackMapTool,
                                                                 #SelectionTool      = trackSelectionTool
                                                                 SplitProbTool      = TrigNnPixelClusterSplitProbTool
                                                                )
#TODO need to add ambiguityScoreProcessor to InDetTrigMTTrkAmbiguityScore_bjet


#end of DenseEnvironmentsAmbiguityScoreProcessorTool

#from TrigInDetConfig.InDetPT import ambiSolvingAlgs
#print("ambiSolvingAlgs print") 
#print(ambiSolvingAlgs)

# Now let's replace the algorithm in the sequence with this one....
myalg.AmbiguityProcessor = ambiguityProcessor
myalg2.AmbiguityScoreProcessor = ambiguityScoreProcessor
from AthenaCommon.Constants import VERBOSE,DEBUG,INFO
myalg.OutputLevel = DEBUG
myalg.lock()
print("Current ambiguity solver")
print(myalg)
print("Current score processor")
print(myalg2)

log.info("Config printing finished")
