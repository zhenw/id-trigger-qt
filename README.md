# ID Trigger QT

Zhen Wang's Qualification Task for ATLAS Inner Detector Trigger 

1.Make sure you have a version of Athena which is up-to-date. Here I used the tag nightly/master/2021-06-27T2101

2.Make a directory near the running folder and put the test_preinclude_NNclustering.py and test_postinclude_NNclustering.py in it.

3.Copy the test_trigID_bjet_pu40_preinclude.py into the folder "athena/Trigger/TrigValidation/TrigInDetValidation/test/" and replace the "test_trigID_bjet_pu40.py" with it.

4.Modify the path/to/ with your directory to include.py files in step2.

5.It's OK to start a test.
