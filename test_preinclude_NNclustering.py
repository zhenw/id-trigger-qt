from TrigInDetConfig.ConfigSettings import getInDetTrigConfig
getInDetTrigConfig("bjet")._newConfig = True
from AthenaCommon.Logging import logging 
log = logging.getLogger("TrigInDetValidation")
log.info( "HELLO HELLO preinclude: test_preinclude_NNclustering.py" ) 

# Turn on NN tracking
from InDetTrigRecExample.InDetTrigFlags import InDetTrigFlags
InDetTrigFlags.doPixelClusterSplitting.set_Value_and_Lock(True)
